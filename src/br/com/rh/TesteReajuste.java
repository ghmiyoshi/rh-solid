package br.com.rh;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import br.com.rh.model.Cargo;
import br.com.rh.model.Funcionario;
import br.com.rh.service.ReajusteService;
import br.com.rh.service.reajustes.ValidacaoPercentualReajuste;

public class TesteReajuste {
	
	public static void main(String[] args) {
		Funcionario gabriel = new Funcionario("Gabriel", "123.456.789-00", Cargo.ANALISTA, new BigDecimal("8000.0"), LocalDate.of(2021, 1, 12));
		
		ValidacaoPercentualReajuste validacaoPercentualReajuste = new ValidacaoPercentualReajuste();
		ReajusteService reajusteService = new ReajusteService(List.of(validacaoPercentualReajuste));
		reajusteService.reajustarSalarioDoFuncionario(gabriel, new BigDecimal("3199.0"));
		System.out.println(gabriel.getDadosPessoais().getSalario());
	}

}
