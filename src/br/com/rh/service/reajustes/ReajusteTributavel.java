package br.com.rh.service.reajustes;

import java.math.BigDecimal;

public interface ReajusteTributavel extends Reajuste {

	BigDecimal valorImpostoDeRenda();

}
