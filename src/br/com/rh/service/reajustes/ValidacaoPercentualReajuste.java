package br.com.rh.service.reajustes;

import java.math.BigDecimal;

import br.com.rh.exception.ValidacaoException;
import br.com.rh.model.Funcionario;

public class ValidacaoPercentualReajuste implements ValidacaoReajuste{

	@Override
	public void validar(BigDecimal aumento, Funcionario funcionario) {
		BigDecimal salarioAtual = funcionario.getDadosPessoais().getSalario();
		BigDecimal percentualReajuste = aumento.divide(salarioAtual);
		if (percentualReajuste.equals(new BigDecimal("0.4")))
			throw new ValidacaoException("Reajuste não pode ser superior a 40% do salario");
	}

}
