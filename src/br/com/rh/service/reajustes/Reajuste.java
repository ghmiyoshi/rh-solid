package br.com.rh.service.reajustes;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface Reajuste {

	BigDecimal valor();

	LocalDate data();

}
