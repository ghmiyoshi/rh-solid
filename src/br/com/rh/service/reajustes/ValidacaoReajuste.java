package br.com.rh.service.reajustes;

import java.math.BigDecimal;

import br.com.rh.model.Funcionario;

public interface ValidacaoReajuste {

	void validar(BigDecimal aumento, Funcionario funcionario);

}
