package br.com.rh.service.reajustes;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

import br.com.rh.exception.ValidacaoException;
import br.com.rh.model.Funcionario;

public class ValidacaoPeriodicidadeReajuste implements ValidacaoReajuste {

	@Override
	public void validar(BigDecimal aumento, Funcionario funcionario) {
		LocalDate dataUltimoReajuste = funcionario.getDataUltimoReajuste();
		LocalDate dataAtual = LocalDate.now();
		int mesesUltimoReajuste = Period.between(dataUltimoReajuste, dataAtual).getMonths();

		if (mesesUltimoReajuste < 6)
			throw new ValidacaoException("Intervalo entre reajustes deve ser maior que 6 meses");
	}

}
