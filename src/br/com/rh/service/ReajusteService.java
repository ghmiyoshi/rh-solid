package br.com.rh.service;

import java.math.BigDecimal;
import java.util.List;

import br.com.rh.model.Funcionario;
import br.com.rh.service.reajustes.ValidacaoReajuste;

public class ReajusteService {

	private List<ValidacaoReajuste> validacoes;

	public ReajusteService(List<ValidacaoReajuste> validacoes) {
		this.validacoes = validacoes;
	}

	public void reajustarSalarioDoFuncionario(Funcionario funcionario, BigDecimal aumento) {
		validacoes.forEach(validacao -> validacao.validar(aumento, funcionario));
		BigDecimal salarioReajustado = funcionario.getDadosPessoais().getSalario().add(aumento);
		funcionario.atualizarSalario(salarioReajustado);
	}

}
